/// <reference types="Cypress" />

context('Actions', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/')
  })

  it('should add Car into list', () => {

    cy.get('[name="id"]').type(Math.floor((Math.random() * 25) + 97));
    cy.get('[name="name"]').type(getRandomString());
    cy.get('[name="shopPrice"]').type(getRandomString());
    cy.get('[name="currency"]').type(getRandomString());  
    cy.get('[name="friendPrice"]').type(getRandomString());
    cy.get('[name="sold"]').type("No");
    cy.get('form > button').click();
    cy.get('table').find('tr').should('have.length', 4)
  });

  it('should remove a car from the list', () => {
    cy.get(':nth-child(1) > :nth-child(6) > button').click();
    cy.get('table').find('tr').should('have.length', 3)
    
  })



  function getRandomString() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for (var i = 0; i < 10; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }
})
