import React, { Component } from 'react';
import Table from './Table';
import Form from './Form';
import axios from 'axios';

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: []
        };
    }

    componentWillMount() {
        axios.get('http://localhost:8080/api/getAllCArs')
        .then((responseJson) => {
          this.setState({ data : responseJson.data })
        })
        .catch((error) => {
          console.error(error);
        })
        .finally(function () {});
    }

    removeCar = index => {

        Promise.resolve(axios.get('http://localhost:8080/api/deleteCar/'+ index , {})
        .then((responseJson) => {
            //this.setState({ data : data })
            return responseJson;
        }))
        .then((data) => {
            this.setState({ data : data.data })
        });
    }

    handleSubmit = car => {
        this.setState({data: [...this.state.data, car]});
    }

    render() {
        const { data } = this.state;
        
        return (
            <div className="container">
                <h1>Car Shop</h1>
                <Table
                    characterData={data}
                    removeCharacter={this.removeCar}
                />
                <h3>Add new car</h3>
                <Form handleSubmit={this.handleSubmit} />
            </div>
        );
    }
}

export default App;