import React, {Component} from 'react';
import axios from 'axios';

class Form extends Component {
    constructor(props) {
        super(props);
        this.initialState = {
            name: '',
            shopPrice: '',
            currency: '',
            friendPrice: '',
            sold: ''
        };

        this.state = this.initialState;
    }

    handleChange = event => {
        const { name, value } = event.target;

        this.setState({
            [name] : value
        });
    }

    onFormSubmit = (event) => {
        event.preventDefault();
        Promise.resolve(axios.post('http://localhost:8080/api/addCar', this.state)
        .then((responseJson) => {
            return responseJson;
        }))
        this.props.handleSubmit(this.state);
        this.setState(this.initialState);
    }

    render() {
        const { id, name, shopPrice, currency, friendPrice, sold } = this.state; 
        return (
            <form onSubmit={this.onFormSubmit}>
                <label>Car ID</label>
                <input 
                    type="text" 
                    name="id" 
                    value={id} 
                    onChange={this.handleChange} />
                <label>Name</label>
                <input 
                    type="text" 
                    name="name" 
                    value={name} 
                    onChange={this.handleChange} />
                <label>Shop price</label>
                <input 
                    type="text" 
                    name="shopPrice" 
                    value={shopPrice} 
                    onChange={this.handleChange} />
                <label>Currency</label>
                <input 
                    type="text" 
                    name="currency" 
                    value={currency} 
                    onChange={this.handleChange} />
                <label>FriendPrice</label>
                <input 
                    type="text" 
                    name="friendPrice" 
                    value={friendPrice} 
                    onChange={this.handleChange} />
                <label>Sold</label>
                <input 
                    type="text" 
                    name="sold" 
                    value={sold} 
                    onChange={this.handleChange} />
                <button type="submit">
                    Submit
                </button>
            </form>
        );
    }
}

export default Form;
